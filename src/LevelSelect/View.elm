module LevelSelect.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Document.Type
  exposing
    ( Document
    )


import Data.Color.Theme.Type
  as Color
import Data.Text.Type
  as Text
import Level.Status.Type
  as Status
import LevelSelect.Message.Type
  exposing
    ( Message (..)
    )
import LevelSelect.Model.Option.Type
  exposing
    ( Option
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import Settings.Color.Theme
  as Theme
import Settings.Language
  as Language
import Settings.Type
  exposing
    ( Settings
    )


drawOption : Settings -> Int -> Option -> Svg a
drawOption settings ix { status } =
  let
    gridX =
      modBy 8 ix
        |> toFloat

    gridY =
      ix // 8
        |> toFloat
  in
    Svg.rect
      [ 0.1 + gridX
        |> String.fromFloat 
        |> SvgAttr.x
      , 0.1 + gridY
        |> String.fromFloat 
        |> SvgAttr.y
      , SvgAttr.width "0.8"
      , SvgAttr.height "0.8"
      , SvgAttr.fill (Theme.toHex settings.theme <| if status == Status.Complete then Color.CompletedBackground else Color.SecondBackground)
      , SvgAttr.stroke (Theme.toHex settings.theme <| if status == Status.Complete then Color.CompletedForeground else Color.SecondForeground)
      , SvgAttr.strokeWidth "0.05"
      ]
      []


drawSelected : Settings -> Int -> Svg a
drawSelected settings ix =
  let
    gridX =
      modBy 8 ix
        |> toFloat

    gridY =
      ix // 8
        |> toFloat
  in
    Svg.rect
      [ 0.1 + gridX
        |> String.fromFloat 
        |> SvgAttr.x
      , 0.1 + gridY
        |> String.fromFloat 
        |> SvgAttr.y
      , SvgAttr.width "0.8"
      , SvgAttr.height "0.8"
      , SvgAttr.fill (Theme.toHex settings.theme Color.SelectedBackground)
      , SvgAttr.stroke (Theme.toHex settings.theme  Color.SelectedForeground)
      , SvgAttr.strokeWidth "0.05"
      ]
      []


view : Settings -> Model -> Document Message
view settings { previous, remaining } =
  let
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.6
        }
      , max =
        { x =
          8.2
        , y =
          8.2
        }
      }
    numPrevious =
      List.length previous
  in
    { title =
      Language.fromText settings.language Text.LevelSelectTitle
    , viewBox =
      viewBox
    , body =
      Svg.rect
        [ SvgAttr.x "-0.3"
        , SvgAttr.y "-0.3"
        , SvgAttr.width "8.2"
        , SvgAttr.height "8.2"
        , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.1"
        ]
        []
      ::
        ( List.indexedMap ((-) (numPrevious - 1) >> drawOption settings) previous
          ++
            ( drawSelected settings numPrevious
              ::
                List.indexedMap
                  ((+) (numPrevious + 1) >> drawOption settings)
                  remaining
            )
        )
    }
