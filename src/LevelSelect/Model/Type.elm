module LevelSelect.Model.Type
  exposing
    ( Model
    , WithLevels
    , complete
    )


import LevelSelect.Model.Option.Type
  as Option
  exposing
    ( Option
    )


type alias Model =
  { selected :
    Option
  , previous :
    List Option
  , remaining :
    List Option
  }


type alias WithLevels m =
  { m
  | levels :
    Model
  }


complete : Model -> Model
complete model =
  case
    model.remaining
  of
    [] ->
      { model
      | selected =
        Option.complete model.selected
      }

    nextLevel :: otherLevels ->
      { model
      | selected =
        nextLevel
      , previous =
        (Option.complete model.selected) :: model.previous
      , remaining =
        otherLevels
      }
