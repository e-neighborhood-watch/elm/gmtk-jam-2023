module LevelSelect.Message.Type
  exposing
    ( Message (..)
    )


type Message
  = Previous
  | Next
  | Select
