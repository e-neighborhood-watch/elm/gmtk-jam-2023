module Settings.Controls.Init
  exposing
    ( init
    )


import Settings.Controls.Type
  exposing
    ( Controls
    )


init : Controls
init =
  { levelSelectPrevious =
    "ArrowLeft"
  , levelSelectNext =
    "ArrowRight"
  , levelSelectSelect =
    "Enter"
  , playerMoveNorth =
    "ArrowUp"
  , playerMoveSouth =
    "ArrowDown"
  , playerMoveEast =
    "ArrowRight"
  , playerMoveWest =
    "ArrowLeft"
  , exitLevel =
    "q"
  , toggleRunLevel =
    " "
  , restartLevel =
    "r"
  , resetLevel =
    "m"
  }
