module Settings.Type
  exposing
    ( Settings
    , WithSettings
    )


import Settings.Color.Theme.Type
  exposing
    ( Theme
    )
import Settings.Controls.Type
  exposing
    ( Controls
    )
import Settings.Delay.Type
  exposing
    ( Delays
    )
import Settings.Font.Size.Type
  exposing
    ( FontSize
    )
import Settings.Language.Type
  exposing
    ( Language
    )


type alias Settings =
  { controls :
    Controls
  , language :
    Language
  , theme :
    Theme
  , fontSize :
    FontSize
  , delay :
    Delays
  }


type alias WithSettings m =
  { m
  | settings :
    Settings
  }
