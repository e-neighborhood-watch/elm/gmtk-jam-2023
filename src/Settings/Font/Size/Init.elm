module Settings.Font.Size.Init
  exposing
    ( init
    )


import Settings.Font.Size.Type
  as FontSize
  exposing
    ( FontSize
    )


init : FontSize
init =
  FontSize.Medium
