module Settings.Delay.Init
  exposing
    ( init
    )


import Settings.Delay.Type
  exposing
    ( Delays
    )


init : Delays
init =
  { slow =
    250
  , normal =
    120
  , fast =
    10
  }
