module Settings.Delay.Type
  exposing
    ( Delays
    )


type alias Delays =
  { slow :
    Int
  , normal :
    Int
  , fast :
    Int
  }
