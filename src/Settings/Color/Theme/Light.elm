module Settings.Color.Theme.Light
  exposing
    ( toHex
    )


import Data.Color.Theme.Type
  as Color
  exposing
    ( ThemeColor
    )


toHex : ThemeColor -> String
toHex color =
  case
    color
  of
    Color.Background ->
      "#CACACA"
    Color.Foreground ->
      "#343434"
    Color.SecondBackground ->
      "#bbbbbb"
    Color.SecondForeground ->
      "#dcdcdc"
    Color.CompletedBackground ->
      "#15b125"
    Color.CompletedForeground ->
      "#55d555"
    Color.SelectedBackground ->
      "#cf0084"
    Color.SelectedForeground ->
      "#ff2eb3"
