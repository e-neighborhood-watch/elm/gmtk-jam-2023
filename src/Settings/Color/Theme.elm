module Settings.Color.Theme
  exposing
    ( toHex
    )


import Data.Color.Theme.Type
  exposing
    ( ThemeColor
    )
import Settings.Color.Theme.Dark
  as Dark
import Settings.Color.Theme.Light
  as Light
import Settings.Color.Theme.Type
  as Theme
  exposing
    ( Theme
    )


toHex : Theme -> ThemeColor -> String
toHex theme =
  case
    theme
  of
    Theme.Dark ->
      Dark.toHex
    Theme.Light ->
      Light.toHex
