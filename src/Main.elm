module Main
  exposing
    ( main
    )


import Browser


import Message.Type
  exposing
    ( Message
    )
import Model.Type
  exposing
    ( Model
    )
import Model.Init
  exposing
    ( init
    )
import Subscriptions
  exposing
    ( subscriptions
    )
import Update
  exposing
    ( update
    )
import View
  exposing
    ( view
    )


main : Program () Model Message
main =
  Browser.document
    { init =
      init
    , subscriptions =
      subscriptions
    , update =
      update
    , view =
      view
    }
