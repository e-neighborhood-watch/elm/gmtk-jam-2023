module Level.Logic.Blocked
  exposing
    ( blocked
    )


import Level.Grammar.Slot.Type
  as Slot
  exposing
    ( Slot (..)
    )
import Level.Grammar.Statement.Type
  as Statement
  exposing
    ( Statement (..)
    )
import Level.Logic.SpaceContains
  exposing
    ( spaceContainsSlot
    )
import Level.Logic.ConditionSlotTrue
  exposing
    ( conditionSlotTrue
    )
import Level.Type
  as Level
  exposing
    ( Level
    )


import Util.Position.Type
  as Position
  exposing
    ( Position
    )


blocked : Level -> Position Int -> List Statement -> Bool
blocked level pos =
  List.any (ruleBlocks level pos)


ruleBlocks : Level -> Position Int -> Statement -> Bool
ruleBlocks level pos rule =
  case
    rule
  of
    IsBlocking slot ->
      spaceContainsSlot level slot pos
    When statementSlot conditionSlot ->
      if
        conditionSlotTrue level conditionSlot
      then
        ruleBlocksSlot level pos statementSlot
      else
        False
    _ ->
      False

ruleBlocksSlot : Level -> Position Int -> Slot Statement -> Bool
ruleBlocksSlot level pos slot =
  case
    slot
  of
    Empty ->
      False
    Filled _ statement ->
      ruleBlocks level pos statement

