module Level.Logic.NounSlotExists
  exposing
    ( nounSlotExists
    )


import Level.Grammar.Noun.Type
  as Noun
  exposing
    ( Noun (..)
    )
import Level.Grammar.Slot.Type
  as Slot
  exposing
    ( Slot (..)
    )


import Level.Logic.SpaceContains
  exposing
    ( spaceContains
    )

import Level.Type
  as Level
  exposing
    ( Level
    )


nounSlotExists : Level -> Slot Noun -> Bool
nounSlotExists level slot =
  case
    slot
  of
    Empty ->
      False
    Filled _ noun ->
      nounExists level noun


{-
slot : (Level -> a -> Bool) -> Level -> Slot a -> Bool
slot f level s =
  case
    s
  of
    Empty ->
      False
    Locked a ->
      f level a
    Filled a ->
      f level a
-}


nounExists : Level -> Noun -> Bool
nounExists level noun =
  List.concatMap
    (\x -> List.map (\y -> { x= x, y= y}) (List.range 0 level.bound.y) )
    (List.range 0 level.bound.x)
  |> List.any (spaceContains level noun)
