module Level.Logic.IsLoss
  exposing
    ( isLoss
    )


import Level.Grammar.Slot.Type
  as Slot
import Level.Grammar.Statement.Type
  as Statement
  exposing
    ( Statement
    )
import Level.Logic.ConditionSlotTrue
  as Logic
import Level.Type
  exposing
    ( Level
    )


isLose : Level -> Statement -> Bool
isLose level s =
  case
    s
  of
    Statement.Lose ->
      True

    Statement.When (Slot.Filled _ statement) conditionSlot ->
      if
        Logic.conditionSlotTrue level conditionSlot
      then
        isLose level statement
      else
        False

    _ ->
      False


isLoss : Level -> Bool
isLoss level =
  List.any (isLose level) level.rules
