module Level.Logic.IsWin
  exposing
    ( isWin
    )


import Level.Grammar.Slot.Type
  as Slot
import Level.Grammar.Statement.Type
  as Statement
  exposing
    ( Statement
    )
import Level.Logic.ConditionSlotTrue
  as Logic
import Level.Type
  exposing
    ( Level
    )


isVictory : Level -> Statement -> Bool
isVictory level s =
  case
    s
  of
    Statement.Victory ->
      True

    Statement.When (Slot.Filled _ statement) conditionSlot ->
      if
        Logic.conditionSlotTrue level conditionSlot
      then
        isVictory level statement
      else
        False

    _ ->
      False


isWin : Level -> Bool
isWin level =
  List.any (isVictory level) level.rules
