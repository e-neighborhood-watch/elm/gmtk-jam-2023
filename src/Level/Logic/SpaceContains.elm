module Level.Logic.SpaceContains
  exposing
    ( spaceContains
    , spaceContainsSlot
    )


import Level.Grammar.Noun.Type
  as Noun
  exposing
    ( Noun (..)
    )
import Level.Grammar.Slot.Type
  as Slot
  exposing
    ( Slot (..)
    )


import Level.Type
  as Level
  exposing
    ( Level
    )


import Util.Map.Get
  as Map
import Util.Position.Type
  as Position
  exposing
    ( Position
    )


spaceContains : Level -> Noun -> Position Int -> Bool
spaceContains level noun pos =
  case
    noun
  of
    Player ->
      level.playerLocation == pos
    Atom thing ->
      case
        Map.get pos level.map
      of
        Just things ->
          List.member thing things
        Nothing ->
          False
    And s1 s2 ->
      spaceContainsSlot level s1 pos || spaceContainsSlot level s2 pos
    On s1 s2 ->
      spaceContainsSlot level s1 pos && spaceContainsSlot level s2 pos


spaceContainsSlot : Level -> Slot Noun -> Position Int -> Bool
spaceContainsSlot level slot pos =
  case
    slot
  of
    Empty ->
      False
    Filled _ noun ->
      spaceContains level noun pos
