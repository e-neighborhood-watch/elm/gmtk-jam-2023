module Level.Logic.ConditionSlotTrue
  exposing
    ( conditionSlotTrue
    )


import Level.Grammar.Condition.Type
  as Condition
  exposing
    ( Condition (..)
    )
import Level.Grammar.Slot.Type
  as Slot
  exposing
    ( Slot (..)
    )


import Level.Logic.NounSlotExists
  exposing
    ( nounSlotExists
    )
import Level.Type
  as Level
  exposing
    ( Level
    )


conditionSlotTrue : Level -> Slot Condition -> Bool
conditionSlotTrue level slot =
  case
    slot
  of
    Empty ->
      False
    Filled _ condition ->
      conditionTrue level condition


conditionTrue : Level -> Condition -> Bool
conditionTrue level condition =
  case
    condition
  of
    Absolute bool ->
      bool
    Not s1 ->
      not (conditionSlotTrue level s1)
    And s1 s2 ->
      conditionSlotTrue level s1 && conditionSlotTrue level s2
    Or s1 s2 ->
      conditionSlotTrue level s1 || conditionSlotTrue level s2
    Exists s1 ->
      nounSlotExists level s1
