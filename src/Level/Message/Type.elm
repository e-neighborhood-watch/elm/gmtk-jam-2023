module Level.Message.Type
  exposing
    ( Message (..)
    )


import Level.Speed.Type
  exposing
    ( Speed
    )
import Util.Direction.Type
  exposing
    ( Direction
    )


type Message
  = Noop
  | Exit
  | Restart
  | Reset
  | Pause
  | Step
  | RunAt Speed Int
  | TimePasses Int
  | Move Direction
  | TriggerMove Int
  | AddPlayerAnimation Int Direction Int
  | SelectFragment Int
  | SelectRule Int
