module Level.View
  exposing
    ( view
    )


import Css
import Svg.Styled
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Animation.Movement.Offset
  as Movement
import Data.Color.Theme.Type
  as Color
import Data.Font.Size
  as Font
import Document.Type
  exposing
    ( Document
    )
import Level.Message.Type
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Pane.Code.View
  as CodePane
import Level.Pane.Moves.View
  as MovesPane
import Level.Pane.Stage.View
  as StagePane
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Language
  as Language
import Settings.Type
  as Settings
  exposing
    ( Settings
    )
import Util.Offset.Type
  exposing
    ( Offset
    )


view : Settings -> Model -> Document Message
view settings ({ level, playerAnimation, holding } as model) =
  let
    playerTransform : String
    playerTransform =
      case
        playerAnimation
      of
        Nothing ->
          ""

        Just animation ->
          let
            animationOffset : Offset Float
            animationOffset =
              Movement.offset animation
          in
            "translate("
              ++ String.fromFloat animationOffset.dx
              ++ " "
              ++ String.fromFloat animationOffset.dy
              ++ ")"
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.5 - Font.rawSize Font.Title settings.fontSize
        }
      , max =
        { x =
        toFloat level.bound.x + {- CodePane.width settings -} 3 + 1.2
        , y =
        toFloat level.bound.y + {- MovesPane.height settings -} 1 + 2.5
        }
      }
  in
    { title =
      Language.fromText settings.language level.title
    , viewBox =
      viewBox
    , body =
      [ Svg.text_
        [ Font.toCss Font.Title settings.fontSize
        , SvgAttr.y "-0.45"
        , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
        ]
        [ Svg.text (Language.fromText settings.language level.title)
        ]
      , Svg.g
        [ SvgAttr.transform ("translate(0 " ++ String.fromFloat (toFloat level.bound.y + 0.4)  ++ ")")
        ]
        ( MovesPane.view settings model )
      , Svg.g
        []
        ( StagePane.view settings level )
      , Svg.use
        [ SvgAttr.xlinkHref "#Character"
        , level.playerLocation.x
          |> String.fromInt
          |> SvgAttr.x
        , level.playerLocation.y
          |> String.fromInt
          |> SvgAttr.y
        , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.transform playerTransform
        ]
        [
        ]
      , Svg.g
        [ SvgAttr.transform ("translate(" ++ String.fromFloat (toFloat level.bound.x + 0.4)  ++ " 0)")
        ]
        ( CodePane.view holding settings level )
      ]
    }
