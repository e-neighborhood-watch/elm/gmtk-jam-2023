module Level.Subscriptions
  exposing
    ( subscriptions
    )


import Browser.Events
  as Browser
import Dict
import Time


import Keys
import Level.Message.Type
  exposing
    ( Message (..)
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Speed.ToDelay
  as Speed
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.Type
  as Direction


subscriptions : Settings -> Model -> Sub Message
subscriptions settings { playerAnimation, running, speed } =
  Sub.batch
    [ case
        playerAnimation
      of
        Nothing ->
          Sub.none

        Just animation ->
          Time.posixToMillis >> TimePasses
            |> Browser.onAnimationFrame
    , Direction.North
      |> Move
      |> Dict.singleton settings.controls.playerMoveNorth
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.South
      |> Move
      |> Dict.singleton settings.controls.playerMoveSouth
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.East
      |> Move
      |> Dict.singleton settings.controls.playerMoveEast
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.West
      |> Move
      |> Dict.singleton settings.controls.playerMoveWest
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Restart
      |> Dict.singleton settings.controls.restartLevel
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Reset
      |> Dict.singleton settings.controls.resetLevel
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Exit
      |> Dict.singleton settings.controls.exitLevel
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , ( case
          running
        of
          Nothing ->
            RunAt speed (Speed.toDelay settings speed)
          Just _ ->
            Pause
      )
      |> Dict.singleton settings.controls.toggleRunLevel
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    ]
