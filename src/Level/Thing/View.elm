module Level.Thing.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Level.Thing.ToString
  as Thing
import Level.Thing.Type
  exposing
    ( Thing
    )
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
import Settings.Type
  exposing
    ( Settings
    )


view : Settings -> (Int, Int) -> Thing -> Svg a
view settings (x, y) thingInItself =
  Svg.use
    [ SvgAttr.xlinkHref ("#" ++ Thing.toString thingInItself)
    , x
      |> String.fromInt
      |> SvgAttr.x
    , y
      |> String.fromInt
      |> SvgAttr.y
    , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
    ]
    []
