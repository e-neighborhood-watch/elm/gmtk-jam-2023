module Level.Thing.ToString
  exposing
    ( toString
    )


import Level.Thing.Type
  exposing
    ( Thing (..)
    )


toString : Thing -> String
toString thingInItself =
  case
    thingInItself
  of
    Trophy ->
      "trophy"
    Apple ->
      "apple"
    Chair ->
      "chair"
    Animal ->
      "animal"
    _ ->
      "TODO"
