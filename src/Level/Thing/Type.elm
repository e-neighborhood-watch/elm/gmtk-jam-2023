module Level.Thing.Type exposing
  ( Thing (..)
  )


type Thing
  = Wall
  | Crate
  | Trophy
  | Chair
  | Apple
  | Animal
  | Kijetesantakalu
