module Level.Thing.List
  exposing
    ( list
    )


import Level.Thing.Type
  exposing
    ( Thing (..)
    )


list : List Thing
list =
  [ Wall
  , Crate
  , Trophy
  , Chair
  , Apple
  , Animal
  , Kijetesantakalu
  ]
