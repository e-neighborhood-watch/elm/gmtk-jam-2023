module Level.Model.Type
  exposing
    ( Model
    , Holding (..)
    )


import Animation.Movement.Type
  as Animation
import Level.Speed.Type
  as Speed
  exposing
    ( Speed
    )
import Level.Type
  exposing
    ( Level
    )


type Holding a
  = NotHolding
  | HoldingFragment a
  | HoldingRule a


type alias Model =
  { level :
    Level
  , restartedLevel :
    Level
  , running :
    Maybe Int
  , playsCount :
    Int
  , holding :
    Holding Int
  , playerAnimation :
    Maybe Animation.Movement
  , speed :
    Speed
  }
