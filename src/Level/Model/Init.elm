module Level.Model.Init
  exposing
    ( init
    )


import Level.Model.Type
  exposing
    ( Model
    , Holding (..)
    )
import Level.Speed.Type
  as Speed
  exposing
    ( Speed
    )
import Level.Type
  exposing
    ( Level
    )


init : Level -> Model
init level =
  { level =
    level
  , restartedLevel =
    level
  , playerAnimation =
    Nothing
  , running =
    Nothing
  , playsCount =
    0
  , speed =
    Speed.Normal
  , holding =
    NotHolding
  }
