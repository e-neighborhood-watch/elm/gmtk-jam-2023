module Level.Pane.Code.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr
import Svg.Styled.Events
  as SvgEvent


import Data.Color.Theme.Type
  as Color
import Data.Font.Size
  as Font
import Level.Grammar.Condition.View
  as Condition
import Level.Grammar.Fragment.View
  as Fragment
import Level.Grammar.Noun.View
  as Noun
import Level.Grammar.Path.View
  as Path
import Level.Grammar.Statement.View
  as Statement
import Level.Message.Type
  as Message
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Holding (..)
    )
import Level.Type
  exposing
    ( Level
    )
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Font.Size.Type
  as FontSize
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )


view : Holding Int -> Settings -> Level -> List (Svg Message)
view holding settings level =
  let
    box =
      Svg.rect
        [ SvgAttr.x "-0.1"
        , SvgAttr.y "-0.1"
        , SvgAttr.height (toFloat level.bound.y + 1.6 |> String.fromFloat)
        , SvgAttr.width "4.2"
        , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.1"
        ]
        []

    codeLines =
      List.indexedMap
        ( \ ix frag ->
          frag
          |> Fragment.view settings
          |> Svg.text
          |> List.singleton
          |> Svg.text_
            [ Font.toCss Font.Text settings.fontSize
            , SvgAttr.fill
              ( if
                 HoldingFragment ix == holding
                then
                  Theme.toHex settings.theme Color.SelectedForeground
                else
                  Theme.toHex settings.theme Color.Foreground
              )
            , SvgAttr.fontWeight "bold"
            , SvgAttr.dominantBaseline "hanging"
            , SvgEvent.onClick (Message.SelectFragment ix)
            , ix
              |> toFloat
              |> (*) (Font.rawSize Font.Text settings.fontSize)
              |> String.fromFloat
              |> SvgAttr.y
            ]
        )
        level.fragments

    separator =
      Svg.line
        [ SvgAttr.x1 "-0.1"
        , SvgAttr.x2 "4.1"
        , SvgAttr.y1 (toFloat level.bound.y/2 + 0.7 |> String.fromFloat)
        , SvgAttr.y2 (toFloat level.bound.y/2 + 0.7 |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.1"
        , SvgAttr.strokeDasharray "0.1"
        , SvgAttr.strokeDashoffset "0.05"
        ]
        []

    rulesLines =
      List.indexedMap
        ( \ ix rule ->
          rule
          |> Statement.view (Condition.view Noun.view) Noun.view Path.view settings
          |> Svg.text
          |> List.singleton
          |> Svg.text_
            [ Font.toCss Font.Text settings.fontSize
            , SvgAttr.fill
              ( if
                 HoldingRule ix == holding
                then
                  Theme.toHex settings.theme Color.SelectedForeground
                else
                  Theme.toHex settings.theme Color.Foreground
              )
            , SvgAttr.fontWeight "bold"
            , SvgAttr.dominantBaseline "hanging"
            , SvgEvent.onClick (Message.SelectRule ix)
            , ix
              |> toFloat
              |> (*) (Font.rawSize Font.Text settings.fontSize)
              |> (+) (toFloat level.bound.y/2 + 0.8)
              |> String.fromFloat
              |> SvgAttr.y
            ]
        )
        level.rules
      ++
        [ Svg.text_
          [ Font.toCss Font.Text settings.fontSize
            , SvgAttr.fill
              ( if
                 HoldingRule (List.length level.rules) == holding
                then
                  Theme.toHex settings.theme Color.SelectedForeground
                else
                  Theme.toHex settings.theme Color.Foreground
              )
          , SvgAttr.fontWeight "bold"
          , SvgAttr.dominantBaseline "hanging"
            , SvgEvent.onClick (Message.SelectRule (List.length level.rules))
          , List.length level.rules
            |> toFloat
            |> (*) (Font.rawSize Font.Text settings.fontSize)
            |> (+) (toFloat level.bound.y/2 + 0.8)
            |> String.fromFloat
            |> SvgAttr.y
          ]
          [ Svg.text "{ ... }"
          ]
        ]
  in
    box :: (codeLines ++ rulesLines ++ [separator])
