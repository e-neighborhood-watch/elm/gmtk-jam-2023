module Level.Pane.Stage.View
  exposing
    ( view
    )


import Dict
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Level.Thing.View
  as Thing
import Level.Type
  exposing
    ( Level
    )
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )


view : Settings -> Level -> List (Svg a)
view settings level =
  Svg.rect
    [ SvgAttr.x "-0.1"
    , SvgAttr.y "-0.1"
    , level.bound.x
      |> toFloat
      |> (+) 0.2
      |> String.fromFloat
      |> SvgAttr.width
    , level.bound.y
      |> toFloat
      |> (+) 0.2
      |> String.fromFloat
      |> SvgAttr.height
    , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
    , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
    , SvgAttr.strokeWidth "0.1"
    ]
    []
    ::
      Dict.foldl
        ( \ k ->
          List.map (Thing.view settings k) >> (++)
        )
        []
        level.map
