module Level.Pane.Moves.View
  exposing
    ( view
    )


import Css
import Html.Styled
  as Html
  exposing
    ( Html
    )
import Html.Styled.Attributes
  as HtmlAttr
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr
import Svg.Styled.Events
  as SvgEvent


import Data.Color.Theme.Type
  as Color
  exposing
    ( ThemeColor
    )
import Level.Logic.IsLoss
  as Logic
import Level.Message.Type
  as Message
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Speed.ToDelay
  as Speed
import Level.Speed.Type
  as Speed
import Level.Type
  exposing
    ( Level
    )
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.ToString
  as Direction
import Util.Direction.Type
  exposing
    ( Direction
    )


imageForMove : Settings -> ThemeColor -> ThemeColor -> Direction -> Html a
imageForMove settings background foreground move =
  Svg.svg
    [ SvgAttr.viewBox "0 0 1 1"
    , SvgAttr.css
      [ Css.height (Css.pct 91)
      , Css.flexShrink Css.zero
      ]
    , SvgAttr.preserveAspectRatio "xMidYMid"
    ]
    [ Svg.use
      [ SvgAttr.xlinkHref ("#" ++ Direction.toString move)
      , SvgAttr.x "0"
      , SvgAttr.y "0"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      , SvgAttr.stroke (Theme.toHex settings.theme foreground)
      ]
      []
    ]


view : Settings -> Model -> List (Svg Message)
view settings { level, speed, running, restartedLevel } =
  let
    atLevelStart : Bool
    atLevelStart =
      level.playerLocation == restartedLevel.playerLocation
        && level.map == restartedLevel.map
        && level.playerMoves == restartedLevel.playerMoves

    lost =
      Logic.isLoss level
  in
    [ Svg.rect
      [ SvgAttr.x "-0.1"
      , SvgAttr.y "-0.1"
      , level.bound.x
        |> toFloat
        |> (+) 0.2
        |> String.fromFloat
        |> SvgAttr.width
      , SvgAttr.height "1.2"
      , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
      , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
      , SvgAttr.strokeWidth "0.1"
      ]
      []
    , Svg.use
      [ SvgAttr.x "0"
      , SvgAttr.y "0"
      , SvgAttr.xlinkHref "#Speed1"
      , SvgAttr.fill <|
        if
          lost
        then
          Theme.toHex settings.theme Color.SecondForeground
        else if
          speed /= Speed.Slow
        then
          Theme.toHex settings.theme Color.SelectedBackground
        else if
          atLevelStart
        then
          Theme.toHex settings.theme Color.SelectedForeground
        else
          Theme.toHex settings.theme Color.CompletedForeground
      , SvgEvent.onClick (Message.RunAt Speed.Slow (Speed.toDelay settings Speed.Slow))
      ]
      []
    , Svg.use
      [ SvgAttr.x "0.5"
      , SvgAttr.y "0"
      , SvgAttr.xlinkHref "#Speed2"
      , SvgAttr.fill <|
        if
          lost
        then
          Theme.toHex settings.theme Color.SecondForeground
        else if
          speed /= Speed.Normal
        then
          Theme.toHex settings.theme Color.SelectedBackground
        else if
          atLevelStart
        then
          Theme.toHex settings.theme Color.SelectedForeground
        else
          Theme.toHex settings.theme Color.CompletedForeground
      , SvgEvent.onClick (Message.RunAt Speed.Normal (Speed.toDelay settings Speed.Normal))
      ]
      []
    , Svg.use
      [ SvgAttr.x "1"
      , SvgAttr.y "0"
      , SvgAttr.xlinkHref "#Speed3"
      , SvgAttr.fill <|
        if
          lost
        then
          Theme.toHex settings.theme Color.SecondForeground
        else if
          speed /= Speed.Fast
        then
          Theme.toHex settings.theme Color.SelectedBackground
        else if
          atLevelStart
        then
          Theme.toHex settings.theme Color.SelectedForeground
        else
          Theme.toHex settings.theme Color.CompletedForeground
      , SvgEvent.onClick (Message.RunAt Speed.Fast (Speed.toDelay settings Speed.Fast))
      ]
      []
    , Svg.use
      [ SvgAttr.x "0"
      , SvgAttr.y "0.5"
      , SvgAttr.xlinkHref "#Pause"
      , SvgAttr.fill <|
        if
          lost
        then
          Theme.toHex settings.theme Color.SecondForeground
        else if
          running == Nothing
            && not atLevelStart
        then
          Theme.toHex settings.theme Color.CompletedForeground
        else
          Theme.toHex settings.theme Color.SelectedForeground
      , SvgEvent.onClick <|
        if
          running == Nothing
        then
          Message.RunAt speed (Speed.toDelay settings speed)
        else
          Message.Pause
      ]
      []
    , Svg.use
      [ SvgAttr.x "0.5"
      , SvgAttr.y "0.5"
      , SvgAttr.xlinkHref "#Step"
      , SvgAttr.fill <|
        if
          lost
        then
          Theme.toHex settings.theme Color.SecondForeground
        else
          Theme.toHex settings.theme Color.SelectedForeground
      , SvgEvent.onClick Message.Step
      ]
      []
    , Svg.use
      [ SvgAttr.x "1"
      , SvgAttr.y "0.5"
      , SvgAttr.xlinkHref "#Stop"
      , SvgAttr.fill <|
        if
          atLevelStart
        then
          Theme.toHex settings.theme Color.SecondForeground
        else if
          lost
        then
          Theme.toHex settings.theme Color.CompletedForeground
        else
          Theme.toHex settings.theme Color.SelectedForeground
      , SvgEvent.onClick Message.Reset
      ]
      []
    , let
        width : Float
        width =
          level.bound.x
          |> toFloat
          |> (+) -1.6
      in
        Svg.svg
          [ SvgAttr.viewBox ("0 0 " ++ String.fromInt (round (width * 100)) ++ " 110")
          , SvgAttr.x "1.65"
          , SvgAttr.y "-0.05"
          , SvgAttr.height "1.1"
          , width
            |> String.fromFloat
            |> SvgAttr.width
          ]
          [ Svg.foreignObject
            [ SvgAttr.x "0"
            , SvgAttr.y "0"
            , SvgAttr.width "100%"
            , SvgAttr.height "100%"
            ]
            [ List.foldl (imageForMove settings Color.SecondBackground Color.SecondForeground >> (::)) [] level.playerMoves.performed
              ++
                ( case
                    level.playerMoves.queued
                  of
                    [] ->
                      []

                    next :: rest ->
                      imageForMove settings Color.SelectedBackground Color.SelectedForeground next
                        :: List.map (imageForMove settings Color.CompletedBackground Color.CompletedForeground) rest
                )
              |>
                Html.div
                  [ HtmlAttr.css
                    [ Css.overflowX Css.scroll
                    , Css.width (Css.pct 100)
                    , Css.height (Css.pct 100)
                    , Css.displayFlex
                    , Css.alignItems Css.center
                    ]
                  , HtmlAttr.id "movesList"
                  ]
            ]
          ]
    , Svg.line
      [ SvgAttr.x1 "1.6"
      , SvgAttr.y1 "-0.1"
      , SvgAttr.x2 "1.6"
      , SvgAttr.y2 "1.1"
      , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
      , SvgAttr.strokeWidth "0.1"
      ]
      []
    ]
