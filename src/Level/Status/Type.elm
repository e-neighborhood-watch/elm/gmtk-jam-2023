module Level.Status.Type
  exposing
    ( Status (..)
    , WithStatus
    , add
    )


import Level.Type
  exposing
    ( Level
    )

type Status
  = Complete
  | Incomplete


type alias WithStatus l =
  { l
  | status :
    Status
  }


add : Status -> Level -> WithStatus Level
add status lvl =
  { status =
    status
  , map =
    lvl.map
  , fragments =
    lvl.fragments
  , rules =
    lvl.rules
  , bound =
    lvl.bound
  , playerLocation =
    lvl.playerLocation
  , playerMoves =
    lvl.playerMoves
  , title =
    lvl.title
  , description =
    lvl.description
  }
