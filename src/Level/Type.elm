module Level.Type
  exposing
    ( Level
    , get
    )


import Data.Text.Type
  exposing
    ( Text
    )
import Level.Grammar.Fragment.Type
  exposing
    ( Fragment
    )
import Level.Grammar.Statement.Type
  exposing
    ( Statement
    )
import Level.Thing.Type
  exposing
    ( Thing
    )
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Position.Type
  exposing
    ( Position
    )


type alias Level =
  { map :
    Map Int (List Thing)
  , fragments :
    List Fragment
  , rules:
    List Statement
  , bound :
    Position Int
  , playerLocation :
    Position Int
  , playerMoves :
    { queued :
      List Direction
    , performed :
      List Direction
    }
  , title :
    Text
  , description :
    Maybe Text
  }


get :
  { a
  | map :
    Map Int (List Thing)
  , fragments :
    List Fragment
  , rules :
    List Statement
  , bound :
    Position Int
  , playerLocation :
    Position Int
  , playerMoves :
    { queued :
      List Direction
    , performed :
      List Direction
    }
  , title :
    Text
  , description :
    Maybe Text
  }
    -> Level
get x =
  { map =
    x.map
  , fragments =
    x.fragments
  , rules =
    x.rules
  , bound =
    x.bound
  , playerLocation =
    x.playerLocation
  , playerMoves =
    x.playerMoves
  , title =
    x.title
  , description =
    x.description
  }
