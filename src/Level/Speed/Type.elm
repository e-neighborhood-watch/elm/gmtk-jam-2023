module Level.Speed.Type
  exposing
    ( Speed (..)
    )


type Speed
  = Slow
  | Normal
  | Fast
