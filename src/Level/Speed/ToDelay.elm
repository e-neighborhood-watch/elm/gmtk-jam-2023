module Level.Speed.ToDelay
  exposing
    ( toDelay
    )


import Level.Speed.Type
  exposing
    ( Speed (..)
    )
import Settings.Type
  exposing
    ( Settings
    )


toDelay : Settings -> Speed -> Int
toDelay { delay } speed =
  case
    speed
  of
    Slow ->
      delay.slow

    Normal ->
      delay.normal

    Fast ->
      delay.fast
