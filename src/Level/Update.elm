module Level.Update
  exposing
    ( update
    )


import Browser.Dom
  as Dom
import Process
import Task
import Time


import Animation.Animated.Update
  as Animated
import Level.Logic.IsLoss
  as Logic
import Level.Logic.IsWin
  as Logic
import Level.Message.Type
  as Message
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    , Holding (..)
    )
import Level.Update.Result.Type
  as Update
import Util.Box.Within
  as Box
import Util.Offset.FromDirection
  as Offset
import Util.Offset.Scale
  as Offset
import Util.Offset.ToFloat
  as Offset
import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Move
  as Position

import Level.Grammar.Validate
  exposing
    ( validate
    )
import Level.Logic.Blocked
  exposing
    ( blocked
    )


popMove : Model -> Update.Result
popMove ({ level } as model) =
  if
    Logic.isLoss level
  then
    Update.NoChange
  else
    case
      level.playerMoves.queued
    of
      [] ->
        Dom.setViewportOf "movesList" (100*List.length level.playerMoves.performed |> toFloat) 0
          |> Task.attempt ( \_ -> Message.Noop )
          |> Update.InternalChange model
      nextMove :: queuedMoves ->
        let
          newLocation =
            Position.move nextMove level.playerLocation

          levelBox =
            { min =
              { x =
                0
              , y =
                0
              }
            , max =
              level.bound
            }

          newMoves =
            { queued =
              queuedMoves
            , performed =
              nextMove :: level.playerMoves.performed
            }

          queueNextMove =
            case
              model.running
            of
              Nothing ->
                -- If we are not running then we are stepping so don't queue a next move
                Cmd.none
              Just millis ->
                millis
                  |> toFloat -- WHY sleep NEED A FLOAT!?
                  |> Process.sleep
                  |> Task.perform (\ () -> Message.TriggerMove model.playsCount)

          addAnimation =
            -- if we are not running then animation should be short since we are stepping
            Time.now
              |> Task.perform (Time.posixToMillis >> Message.AddPlayerAnimation (Maybe.withDefault 50 model.running) nextMove)

          scrollToNextMove =
            Dom.setViewportOf "movesList" (100*List.length newMoves.performed - 220 |> toFloat) 0
              |> Task.attempt ( \ _ -> Message.Noop )

        in
          if
            Box.within levelBox newLocation && not (blocked level newLocation level.rules)
          then
            let
              newLevel =
                { level
                | playerMoves =
                  newMoves
                , playerLocation =
                  newLocation
                }
            in
              if
                Logic.isLoss newLevel
              then
                Cmd.batch
                  [ queueNextMove
                  , addAnimation
                  , scrollToNextMove
                  ]
                  |>
                    Update.InternalChange
                      { model
                      | level =
                        newLevel
                      }
              else if
                Logic.isWin newLevel
              then
                Update.Win
              else
                Cmd.batch
                  [ queueNextMove
                  , addAnimation
                  , scrollToNextMove
                  ]
                  |>
                    Update.InternalChange
                      { model
                      | level =
                        newLevel
                      }
          else
            let
              newLevel =
                { level
                | playerMoves =
                  newMoves
                }
            in
              if
                Logic.isLoss newLevel
              then
                Cmd.batch
                  [ queueNextMove
                  , scrollToNextMove
                  ]
                  |>
                    Update.InternalChange
                      { model
                      | level =
                        newLevel
                      }
              else if
                Logic.isWin newLevel
              then
                Update.Win
              else
                Cmd.batch
                  [ queueNextMove
                  , scrollToNextMove
                  ]
                  |>
                    Update.InternalChange
                      { model
                      | level =
                        newLevel
                      }


update : Message -> Model -> Update.Result
update msg ({ level, playerAnimation, running } as model) =
  case
    msg
  of
    Message.Noop ->
      Update.NoChange

    Message.Exit ->
      Update.Exit

    Message.Pause ->
      Update.InternalChange
        { model
        | running =
          Nothing
        }
        Cmd.none

    Message.Reset ->
      Dom.setViewportOf "movesList" 0 0
        |> Task.attempt ( \ _ -> Message.Noop )
        |>
          Update.InternalChange
            { model
            | level =
              { level
              | map =
                model.restartedLevel.map
              , playerLocation =
                model.restartedLevel.playerLocation
              , playerMoves =
                model.restartedLevel.playerMoves
              }
            , running =
              Nothing
            , playsCount =
              model.playsCount + 1
            , playerAnimation =
              Nothing
            }

    Message.Restart ->
      Dom.setViewportOf "movesList" 0 0
        |> Task.attempt ( \ _ -> Message.Noop )
        |>
          Update.InternalChange
            { model
            | level =
              model.restartedLevel
            , running =
              Nothing
            , playsCount =
              model.playsCount + 1
            , playerAnimation =
              Nothing
            }

    Message.Step ->
      popMove
        { model
        | running =
          Nothing
        }

    Message.TriggerMove playId ->
      if
        playId == model.playsCount
        && running /= Nothing
      then
        popMove model
      else
        Update.NoChange

    Message.RunAt speed millis ->
      case
        running
      of
        Just oldMillis ->
          if
            speed == model.speed
              && millis == oldMillis
          then
            Update.NoChange
          else
            Update.InternalChange
              { model
              | running =
                Just millis
              , speed =
                speed
              }
              Cmd.none
        Nothing ->
          popMove
            { model
            | running =
              Just millis
            , speed =
              speed
            , playsCount =
              model.playsCount + 1
            }

    Message.TimePasses newTime ->
      case
        playerAnimation
      of
        Nothing ->
          Update.NoChange

        Just animation ->
          Update.InternalChange
            { model
            | playerAnimation =
              Animated.update newTime animation
            }
            Cmd.none

    Message.Move dir ->
      let
        newLocation =
          Position.move dir level.playerLocation
        levelBox =
          { min =
            { x =
              0
            , y =
              0
            }
          , max =
            level.bound
          }
      in
        if
          Box.within levelBox newLocation
        then
          Time.now
            |> Task.perform (Time.posixToMillis >> Message.AddPlayerAnimation 50 dir)
            |>
              Update.InternalChange
                { model
                | level =
                  { level
                  | playerLocation =
                    Position.move dir level.playerLocation
                  }
                }
        else
          Update.NoChange

    Message.SelectFragment ix ->
      case
        model.holding
      of
        NotHolding ->
          Update.InternalChange
            { model
            | holding =
              HoldingFragment ix
            }
            Cmd.none
        HoldingFragment _ ->
          Update.InternalChange
            { model
            | holding =
              HoldingFragment ix
            }
            Cmd.none
        HoldingRule rix ->
          -- TODO
          Update.NoChange

    Message.SelectRule ix ->
      case
        model.holding
      of
        NotHolding ->
          Update.InternalChange
            { model
            | holding =
              HoldingRule ix
            }
            Cmd.none
        HoldingRule _ ->
          Update.InternalChange
            { model
            | holding =
              HoldingRule ix
            }
            Cmd.none
        HoldingFragment rix ->
          -- TODO
          Update.NoChange

    Message.AddPlayerAnimation duration dir currentTime ->
      let
        offset : Offset Float
        offset =
          dir
            |> Offset.fromDirection
            |> Offset.scale -1
            |> Offset.toFloat
      in
        Update.InternalChange
          { model
          | playerAnimation =
            Just
              { currentTime =
                currentTime
              , duration =
                duration
              , endingTime =
                duration + currentTime
              , dx =
                offset.dx
              , dy =
                offset.dy
              }
          }
          Cmd.none
