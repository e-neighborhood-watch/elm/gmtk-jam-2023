module Level.Grammar.Condition.Insert
  exposing
    ( insert
    )


import Util.Maybe.OrElse
  as Maybe


import Level.Grammar.Condition.Type
  as Condition
  exposing
    ( Condition
    )
import Level.Grammar.Fragment.Type
  as Fragment
  exposing
    ( Fragment
    )
import Level.Grammar.Noun.Type
  exposing
    ( Noun
    )
import Level.Grammar.Slot.Type
  as Slot


insert : (Noun -> Fragment -> Maybe Noun) -> Condition -> Fragment -> Maybe Condition
insert insertNoun condition fragment =
  case
    condition
  of
    Condition.Absolute _ ->
      Nothing

    Condition.Not slot ->
      case
        slot
      of
        Slot.Empty ->
          case
            fragment
          of
            Fragment.Condition innerCondition ->
              innerCondition
              |> Slot.Filled False
              |> Condition.Not
              |> Just
            _ ->
              Nothing
        Slot.Filled lock slotCondition ->
          insert insertNoun slotCondition fragment
          |> Maybe.map (Slot.Filled lock >> Condition.Not)

    Condition.And slot1 slot2 ->
      let
        slot1Result =
          case
            slot1
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Condition innerCondition ->
                  innerCondition
                  |> Slot.Filled False
                  |> (\ newSlot -> Condition.And newSlot slot2)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock innerCondition ->
              insert insertNoun innerCondition fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Condition.And newSlot slot2)
        slot2Result =
          case
            slot2
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Condition innerCondition ->
                  innerCondition
                  |> Slot.Filled False
                  |> (\ newSlot -> Condition.And slot1 newSlot)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock innerCondition ->
              insert insertNoun innerCondition fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Condition.And slot1 newSlot)
      in
        Maybe.orElse slot1Result slot2Result

    Condition.Or slot1 slot2 ->
      let
        slot1Result =
          case
            slot1
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Condition innerCondition ->
                  innerCondition
                  |> Slot.Filled False
                  |> (\ newSlot -> Condition.Or newSlot slot2)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock slotCondition ->
              insert insertNoun slotCondition fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Condition.Or newSlot slot2)
        slot2Result =
          case
            slot2
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Condition innerCondition ->
                  innerCondition
                  |> Slot.Filled False
                  |> (\ newSlot -> Condition.Or slot1 newSlot)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock slotCondition ->
              insert insertNoun slotCondition fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Condition.Or slot1 newSlot)
      in
        Maybe.orElse slot1Result slot2Result

    Condition.Exists slot ->
      case
        slot
      of
        Slot.Empty ->
          case
            fragment
          of
            Fragment.Noun noun ->
              noun
              |> Slot.Filled False
              |> Condition.Exists
              |> Just
            _ ->
              Nothing
        Slot.Filled lock noun ->
          insertNoun noun fragment
          |> Maybe.map (Slot.Filled lock >> Condition.Exists)
