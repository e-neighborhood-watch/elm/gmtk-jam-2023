module Level.Grammar.Condition.Type
  exposing
    ( Condition (..)
    )


import Level.Grammar.Slot.Type
  as Slot
  exposing
    ( Slot
    )
import Level.Grammar.Noun.Type
  as Noun
  exposing
    ( Noun
    )


type Condition
  = Absolute Bool
  | Not (Slot Condition)
  | And (Slot Condition) (Slot Condition)
  | Or (Slot Condition) (Slot Condition)
  | Exists (Slot Noun)
--| Done
