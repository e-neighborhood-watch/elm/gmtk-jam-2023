module Level.Grammar.Condition.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Level.Grammar.Condition.Type
  as Condition
  exposing
    ( Condition
    )
import Level.Grammar.Noun.Type
  exposing
    ( Noun
    )
import Level.Grammar.Slot.View
  as Slot
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Font.Size.Type
  as FontSize


view : (Settings -> Noun -> String) -> Settings -> Condition -> String
view nounView settings condition =
  case
    condition
  of
    Condition.Absolute True ->
      "( true )"

    Condition.Absolute False ->
      "( false )"

    Condition.Not slot ->
      String.concat
        [ "( not"
        , Slot.view "(" ")" (view nounView) settings slot
        , " )"
        ]

    Condition.And slot1 slot2 ->
      String.concat
        [ "( "
        , Slot.view "(" ")" (view nounView) settings slot1
        , " and "
        , Slot.view "(" ")" (view nounView) settings slot2
        , " )"
        ]

    Condition.Or slot1 slot2 ->
      String.concat
        [ "( "
        , Slot.view "(" ")" (view nounView) settings slot1
        , " or "
        , Slot.view "(" ")" (view nounView) settings slot2
        , " )"
        ]

    Condition.Exists slot ->
      String.concat
        [ "( "
        , Slot.view "[" "]" nounView settings slot
        , " exists )"
        ]
