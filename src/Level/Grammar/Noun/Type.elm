module Level.Grammar.Noun.Type
  exposing
    ( Noun (..)
    )


import Level.Grammar.Slot.Type
  as Slot
  exposing
    ( Slot
    )
import Level.Grammar.Path.Type
  as Path
  exposing
    ( Path
    )


import Level.Thing.Type
  as Thing
  exposing
    ( Thing
    )


type Noun
  = Atom Thing
  | Player
  | And (Slot Noun) (Slot Noun)
  | On (Slot Noun) (Slot Noun)
--| Anything
--| AnythingExcept (Slot Noun)
--| Alone (Slot Noun)
--| LocatedFrom (Slot Noun) (Slot Path) (Slot Noun)
