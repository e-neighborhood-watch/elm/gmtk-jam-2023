module Level.Grammar.Noun.Insert
  exposing
    ( insert
    )


import Util.Maybe.OrElse
  as Maybe


import Level.Grammar.Fragment.Type
  as Fragment
  exposing
    ( Fragment
    )
import Level.Grammar.Noun.Type
  as Noun
  exposing
    ( Noun
    )
import Level.Grammar.Fragment.Type
  as Fragment
  exposing
    ( Fragment
    )
import Level.Grammar.Slot.Type
  as Slot


insert : Noun -> Fragment -> Maybe Noun
insert noun fragment =
  case
    noun
  of
    Noun.Atom _ ->
      Nothing

    Noun.Player ->
      Nothing

    Noun.And slot1 slot2 ->
      let
        slot1Result =
          case
            slot1
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Noun innerNoun ->
                  innerNoun
                  |> Slot.Filled False
                  |> (\ newSlot -> Noun.And newSlot slot2)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock slotNoun ->
              insert slotNoun fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Noun.And newSlot slot2)
        slot2Result =
          case
            slot2
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Noun innerNoun ->
                  innerNoun
                  |> Slot.Filled False
                  |> (\ newSlot -> Noun.And slot1 newSlot)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock slotNoun ->
              insert slotNoun fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Noun.And slot1 newSlot)
      in
        Maybe.orElse slot1Result slot2Result
