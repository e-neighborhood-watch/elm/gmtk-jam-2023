module Level.Grammar.Noun.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Level.Grammar.Noun.Type
  as Noun
  exposing
    ( Noun
    )
import Level.Grammar.Slot.View
  as Slot
import Level.Thing.Type
  as Thing
import Level.Thing.ToString
  as Thing
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Font.Size.Type
  as FontSize


view : Settings -> Noun -> String
view settings noun =
  case
    noun
  of
    Noun.Player ->
      "[ player ]"
    Noun.And slot1 slot2 ->
      String.concat
        [ "[ "
        , Slot.view "[" "]" view settings slot1
        , " and "
        , Slot.view "[" "]" view settings slot2
        , " ]"
        ]
    Noun.On slot1 slot2 ->
      String.concat
        [ "[ "
        , Slot.view "[" "]" view settings slot1
        , " on "
        , Slot.view "[" "]" view settings slot2
        , " ]"
        ]
    Noun.Atom thing ->
      "[ " ++ Thing.toString thing ++ " ]"

