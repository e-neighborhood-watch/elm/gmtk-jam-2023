module Level.Grammar.Slot.Type
  exposing
    ( Slot (..)
    )


type Slot a
  = Empty
  | Filled Bool a
