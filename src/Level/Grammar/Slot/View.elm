module Level.Grammar.Slot.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Level.Grammar.Slot.Type
  as Slot
  exposing
    ( Slot
    )
import Settings.Type
  exposing
    ( Settings
    )
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Font.Size.Type
  as FontSize


view : String -> String -> (Settings -> a -> String) -> Settings -> Slot a -> String
view openP closeP subView settings slot =
  case
    slot
  of
    Slot.Empty ->
      openP ++ " ... " ++ closeP
    Slot.Filled locked sub ->
      subView settings sub
