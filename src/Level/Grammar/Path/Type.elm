module Level.Grammar.Path.Type
  exposing
    ( Path (..)
    )

import Level.Grammar.Slot.Type
  as Slot
  exposing
    ( Slot
    )

import Util.Direction.Type
  as Direction
  exposing
    ( Direction
    )

type Path
  = Step Direction
  | PlayerDirection
  | Then (Slot Path) (Slot Path)
