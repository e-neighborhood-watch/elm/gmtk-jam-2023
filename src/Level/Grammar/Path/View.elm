module Level.Grammar.Path.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Level.Grammar.Path.Type
  as Path
  exposing
    ( Path
    )
import Level.Grammar.Slot.View
  as Slot
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.Type
  as Direction
  exposing
    ( Direction (..)
    )
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Font.Size.Type
  as FontSize


view : Settings -> Path -> String
view settings path =
  case
    path
  of
    Path.Step North ->
      "< north >"

    Path.Step East ->
      "< east >"

    Path.Step South ->
      "< south >"

    Path.Step West ->
      "< west >"

    Path.PlayerDirection ->
      "< player direction >"

    Path.Then slot1 slot2 ->
      String.concat
        [ "< "
        , Slot.view "<" ">" view settings slot1
        , " then "
        , Slot.view "<" ">" view settings slot2
        , " >"
        ]
