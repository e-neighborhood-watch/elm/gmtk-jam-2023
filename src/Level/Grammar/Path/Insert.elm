module Level.Grammar.Path.Insert
  exposing
    ( insert
    )


import Util.Maybe.OrElse
  as Maybe


import Level.Grammar.Fragment.Type
  as Fragment
  exposing
    ( Fragment
    )
import Level.Grammar.Path.Type
  as Path
  exposing
    ( Path
    )
import Level.Grammar.Fragment.Type
  as Fragment
  exposing
    ( Fragment
    )
import Level.Grammar.Slot.Type
  as Slot


insert : Path -> Fragment -> Maybe Path
insert path fragment =
  case
    path
  of
    Path.Step _ ->
      Nothing

    Path.PlayerDirection ->
      Nothing

    Path.Then slot1 slot2 ->
      let
        slot1Result =
          case
            slot1
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Path innerPath ->
                  innerPath
                  |> Slot.Filled False
                  |> (\ newSlot -> Path.Then newSlot slot2)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock slotPath ->
              insert slotPath fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Path.Then newSlot slot2)
        slot2Result =
          case
            slot2
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Path innerPath ->
                  innerPath
                  |> Slot.Filled False
                  |> (\ newSlot -> Path.Then slot1 newSlot)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock slotPath ->
              insert slotPath fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Path.Then slot1 newSlot)
      in
        Maybe.orElse slot1Result slot2Result
