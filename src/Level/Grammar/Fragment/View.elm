module Level.Grammar.Fragment.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Level.Grammar.Noun.View
  as Noun
import Level.Grammar.Condition.View
  as Condition
import Level.Grammar.Statement.View
  as Statement
import Level.Grammar.Path.View
  as Path
import Level.Grammar.Fragment.Type
  as Fragment
  exposing
    ( Fragment
    )
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )


view : Settings -> Fragment -> String
view settings fragment =
  case
    fragment
  of
    Fragment.Noun noun ->
      Noun.view settings noun
    Fragment.Condition condition ->
      Condition.view Noun.view settings condition
    Fragment.Statement statement ->
      Statement.view
        (Condition.view Noun.view)
        Noun.view
        Path.view
        settings
        statement
    Fragment.Path path ->
      Path.view settings path
