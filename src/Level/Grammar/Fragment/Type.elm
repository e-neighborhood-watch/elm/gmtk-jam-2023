module Level.Grammar.Fragment.Type
  exposing
    ( Fragment (..)
    )

import Level.Grammar.Noun.Type
  exposing
    ( Noun
    )
import Level.Grammar.Path.Type
  exposing
    ( Path
    )
import Level.Grammar.Condition.Type
  exposing
    ( Condition
    )
import Level.Grammar.Statement.Type
  exposing
    ( Statement
    )

type Fragment
  = Noun Noun
  | Condition Condition
  | Statement Statement
  | Path Path
