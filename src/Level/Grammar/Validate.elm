module Level.Grammar.Validate
  exposing
    ( validate
    )


import Level.Grammar.Fragment.Type
  as Fragment
  exposing
    ( Fragment (..)
    )
import Level.Grammar.Slot.Type
  as Slot
  exposing
    ( Slot (..)
    )
import Level.Grammar.Noun.Type
  as Noun
  exposing
    ( Noun (..)
    )
import Level.Grammar.Path.Type
  as Path
  exposing
    ( Path (..)
    )
import Level.Grammar.Condition.Type
  as Condition
  exposing
    ( Condition (..)
    )
import Level.Grammar.Statement.Type
  as Statement
  exposing
    ( Statement (..)
    )


validate : List Fragment -> Maybe (List Statement)
validate fragments =
  case
    fragments
  of
    [] ->
      Just []
    head :: tail ->
      case
        validate tail
      of
        Nothing ->
          Nothing
        Just statements ->
          case
            head
          of
            Statement s ->
              if
                validStatement s
              then
                Just (s :: statements)
              else
                Nothing
            _ ->
              Nothing

validPath : Path -> Bool
validPath path =
  case
    path
  of
    Then s1 s2 ->
      validSlot validPath s1 && validSlot validPath s2
    _ ->
      True


validNoun : Noun -> Bool
validNoun noun =
  case
    noun
  of
    Noun.And s1 s2 ->
      validSlot validNoun s1 && validSlot validNoun s2
    _ ->
      True


validCondition : Condition -> Bool
validCondition condition =
  case
    condition
  of
    Not s1 ->
      validSlot validCondition s1
    Condition.And s1 s2 ->
      validSlot validCondition s1 && validSlot validCondition s1
    Or s1 s2 ->
      validSlot validCondition s1 && validSlot validCondition s1
    Exists s1 ->
      validSlot validNoun s1
    _ ->
      True


validStatement : Statement -> Bool
validStatement statement =
  case
    statement
  of
    IsBlocking s1 ->
      validSlot validNoun s1
    Becomes s1 s2 ->
      validSlot validNoun s1 && validSlot validNoun s2
    Moves s1 s2 ->
      validSlot validNoun s1 && validSlot validPath s2
    When s1 s2 ->
      validSlot validStatement s1 && validSlot validCondition s2
    _ ->
      True

validSlot : (a -> Bool) -> Slot a -> Bool
validSlot validator slot =
  case
    slot
  of
    Empty ->
      False
    Filled _ a ->
      validator a
