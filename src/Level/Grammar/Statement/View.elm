module Level.Grammar.Statement.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Level.Grammar.Condition.Type
  exposing
    ( Condition
    )
import Level.Grammar.Noun.Type
  exposing
    ( Noun
    )
import Level.Grammar.Path.Type
  exposing
    ( Path
    )
import Level.Grammar.Statement.Type
  as Statement
  exposing
    ( Statement
    )
import Level.Grammar.Slot.View
  as Slot
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Font.Size.Type
  as FontSize
import Util.Direction.Type
  as Direction
  exposing
    ( Direction (..)
    )


view
  :  (Settings -> Condition -> String)
  -> (Settings -> Noun -> String)
  -> (Settings -> Path -> String)
  -> Settings
  -> Statement
  -> String
view conditionView nounView pathView settings statement =
  case
    statement
  of
    Statement.Lose ->
      "{ lose }"

    Statement.Victory ->
      "{ victory }"

    Statement.IsBlocking slot ->
      String.concat
        [ "{ "
        , Slot.view "[" "]" nounView settings slot
        , " is blocking }"
        ]

    Statement.Becomes slot1 slot2 ->
      String.concat
        [ "{ "
        , Slot.view "[" "]" nounView settings slot1
        , " becomes "
        , Slot.view "[" "]" nounView settings slot2
        , " }"
        ]

    Statement.Moves slot1 slot2 ->
      String.concat
        [ "{ "
        , Slot.view "[" "]" nounView settings slot1
        , " moves "
        , Slot.view "<" ">" pathView settings slot2
        , " }"
        ]

    Statement.When slot1 slot2 ->
      String.concat
        [ "{ "
        , Slot.view "{" "}" (view conditionView nounView pathView) settings slot1
        , " when "
        , Slot.view "(" ")" conditionView settings slot2
        , " }"
        ]
