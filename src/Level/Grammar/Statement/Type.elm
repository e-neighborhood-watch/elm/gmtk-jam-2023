module Level.Grammar.Statement.Type
  exposing
    ( Statement (..)
    )


import Level.Grammar.Slot.Type
  as Slot
  exposing
    ( Slot
    )
import Level.Grammar.Noun.Type
  as Noun
  exposing
    ( Noun
    )
import Level.Grammar.Path.Type
  as Path
  exposing
    ( Path
    )
import Level.Grammar.Condition.Type
  as Path
  exposing
    ( Condition
    )


type Statement
  = Victory
  | IsBlocking (Slot Noun)
  | Lose
  | Becomes (Slot Noun) (Slot Noun)
  | Moves (Slot Noun) (Slot Path)
  | When (Slot Statement) (Slot Condition)
--| IsPushable (Slot Noun)
--| BecomesWhenOn (Slot Noun) (Slot Noun)
--| Destroys (Slot Noun) (Slot Noun)
--| Produces (Slot Noun) (Slot Noun)
