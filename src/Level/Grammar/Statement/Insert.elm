module Level.Grammar.Statement.Insert
  exposing
    ( insert
    )


import Util.Maybe.OrElse
  as Maybe


import Level.Grammar.Condition.Insert
  as Condition
import Level.Grammar.Fragment.Type
  as Fragment
  exposing
    ( Fragment
    )
import Level.Grammar.Noun.Insert
  as Noun
import Level.Grammar.Path.Insert
  as Path
import Level.Grammar.Slot.Type
  as Slot
import Level.Grammar.Statement.Type
  as Statement
  exposing
    ( Statement
    )


insert : Statement -> Fragment -> Maybe Statement
insert statement fragment =
  case
    statement
  of
    Statement.Victory ->
      Nothing

    Statement.IsBlocking slot ->
      case
        slot
      of
        Slot.Empty ->
          case
            fragment
          of
            Fragment.Noun noun ->
              noun
              |> Slot.Filled False
              |> Statement.IsBlocking
              |> Just
            _ ->
              Nothing
        Slot.Filled lock noun ->
          Noun.insert noun fragment
          |> Maybe.map (Slot.Filled lock >> Statement.IsBlocking)

    Statement.Lose ->
      Nothing

    Statement.Becomes slot1 slot2 ->
      let
        slot1Result =
          case
            slot1
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Noun noun ->
                  noun
                  |> Slot.Filled False
                  |> (\ newSlot -> Statement.Becomes newSlot slot2)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock noun ->
              Noun.insert noun fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Statement.Becomes newSlot slot2)
        slot2Result =
          case
            slot2
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Noun noun ->
                  noun
                  |> Slot.Filled False
                  |> (\ newSlot -> Statement.Becomes slot1 newSlot)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock noun ->
              Noun.insert noun fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Statement.Becomes slot1 newSlot)
      in
        Maybe.orElse slot1Result slot2Result

    Statement.Moves slot1 slot2 ->
      let
        slot1Result =
          case
            slot1
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Noun noun ->
                  noun
                  |> Slot.Filled False
                  |> (\ newSlot -> Statement.Moves newSlot slot2)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock noun ->
              Noun.insert noun fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Statement.Moves newSlot slot2)
        slot2Result =
          case
            slot2
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Path path ->
                  path
                  |> Slot.Filled False
                  |> (\ newSlot -> Statement.Moves slot1 newSlot)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock path ->
              Path.insert path fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Statement.Moves slot1 newSlot)
      in
        Maybe.orElse slot1Result slot2Result

    Statement.When slot1 slot2 ->
      let
        slot1Result =
          case
            slot1
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Statement innerStatement ->
                  innerStatement
                  |> Slot.Filled False
                  |> (\ newSlot -> Statement.When newSlot slot2)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock slotStatement ->
              insert slotStatement fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Statement.When newSlot slot2)
        slot2Result =
          case
            slot2
          of
            Slot.Empty ->
              case
                fragment
              of
                Fragment.Condition condition ->
                  condition
                  |> Slot.Filled False
                  |> (\ newSlot -> Statement.When slot1 newSlot)
                  |> Just
                _ ->
                  Nothing
            Slot.Filled lock condition ->
              Condition.insert Noun.insert condition fragment
              |> Maybe.map (Slot.Filled lock)
              |> Maybe.map (\ newSlot -> Statement.When slot1 newSlot)
      in
        Maybe.orElse slot1Result slot2Result

