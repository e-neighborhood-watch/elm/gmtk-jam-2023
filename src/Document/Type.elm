module Document.Type
  exposing
    ( Document
    )


import Svg.Styled
  exposing
    ( Svg
    )
import Util.Box.Type
  exposing
    ( Box
    )


type alias Document a =
  { title :
    String
  , viewBox :
    Box Float
  , body :
    List (Svg a)
  }
