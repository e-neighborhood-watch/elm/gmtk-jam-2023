module Data.Color.Theme.Type
  exposing
    ( ThemeColor (..)
    )


type ThemeColor
  = Background
  | Foreground
  | SecondBackground
  | SecondForeground
  | CompletedBackground
  | CompletedForeground
  | SelectedBackground
  | SelectedForeground
