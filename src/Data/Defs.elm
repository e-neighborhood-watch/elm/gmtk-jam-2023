module Data.Defs
  exposing
    ( defs
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Level.Thing.ToString
  as Thing
import Level.Thing.List
  as Thing
import Util.Direction.List
  as Direction
import Util.Direction.ToString
  as Direction


defs : Svg a
defs =
  [ Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/SettingsWheel.svg#content"
    , SvgAttr.id "SettingsWheel"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/CancelIcon.svg#content"
    , SvgAttr.id "CancelIcon"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/AcceptIcon.svg#content"
    , SvgAttr.id "AcceptIcon"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Character.svg#content"
    , SvgAttr.id "Character"
    , SvgAttr.width "1"
    , SvgAttr.height "1"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Button/Speed1.svg#content"
    , SvgAttr.id "Speed1"
    , SvgAttr.width "0.5"
    , SvgAttr.height "0.5"
    ]
    []
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Button/Speed2.svg#content"
    , SvgAttr.id "Speed2"
    , SvgAttr.width "0.5"
    , SvgAttr.height "0.5"
    ]
    []
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Button/Speed3.svg#content"
    , SvgAttr.id "Speed3"
    , SvgAttr.width "0.5"
    , SvgAttr.height "0.5"
    ]
    []
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Button/Step.svg#content"
    , SvgAttr.id "Step"
    , SvgAttr.width "0.5"
    , SvgAttr.height "0.5"
    ]
    []
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Button/Pause.svg#content"
    , SvgAttr.id "Pause"
    , SvgAttr.width "0.5"
    , SvgAttr.height "0.5"
    ]
    []
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Button/Stop.svg#content"
    , SvgAttr.id "Stop"
    , SvgAttr.width "0.5"
    , SvgAttr.height "0.5"
    ]
    []
  ]
    ++
      List.map
        ( \ dir ->
          let
            dirStr =
              Direction.toString dir
          in
            Svg.use
              [ SvgAttr.xlinkHref ("Data/Assets/Direction/" ++ dirStr ++ ".svg#content")
              , SvgAttr.id dirStr
              , SvgAttr.width "1"
              , SvgAttr.height "1"
              ]
              []
        )
        Direction.list
    ++
      List.map
        ( \ thingInItself ->
          let
            thingId =
              Thing.toString thingInItself
          in
            Svg.use
              [ SvgAttr.xlinkHref ("Data/Assets/Thing/" ++ thingId ++ ".svg#content")
              , SvgAttr.id thingId
              , SvgAttr.width "1"
              , SvgAttr.height "1"
              ]
              []
        )
        Thing.list
    |> Svg.defs []
