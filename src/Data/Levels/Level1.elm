module Data.Levels.Level1
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Text.Type
  as Text
import Level.Grammar.Condition.Type
  exposing
    ( Condition (..)
    )
import Level.Grammar.Fragment.Type
  exposing
    ( Fragment (..)
    )
import Level.Grammar.Noun.Type
  exposing
    ( Noun (..)
    )
import Level.Grammar.Path.Type
  exposing
    ( Path (..)
    )
import Level.Grammar.Slot.Type
  exposing
    ( Slot (..)
    )
import Level.Grammar.Statement.Type
  exposing
    ( Statement (..)
    )
import Level.Type
  exposing
    ( Level
    )
import Level.Thing.Type
  as Thing
import Util.Direction.Type
  as Direction


level : Level
level =
  { map =
    Dict.singleton (3,3) [Thing.Animal]
  , fragments =
    [ Noun Player
    , Path (Then Empty Empty)
    , Statement Victory
    ]
  , rules =
    []
  , bound =
    { x =
      7
    , y =
      5
    }
  , playerLocation =
    { x =
      0
    , y =
      0
    }
  , playerMoves =
    { queued =
      [ Direction.East
      , Direction.South
      , Direction.East
      , Direction.South
      , Direction.North
      , Direction.South
      , Direction.South
      , Direction.South
      ]
    , performed =
      []
    }
  , title =
    Text.Level1Title
  , description =
    Nothing
  }
